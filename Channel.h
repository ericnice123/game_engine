#ifndef CHANNEL_H
#define CHANNEL_H

#include <vector>
#include "Keyframe.h"
#include "token.h"
#include "Matrix4.h"
#include "Vector4.h"
#include <stdio.h>
#include <iostream>
#include <math.h>

using namespace std;

class Channel
{
private:
    vector<Keyframe> keyframes;
    int numOfKeys;
    char extra_in[256];
    char extra_out[256];
    
public:
    float Evaluate(float time);
    bool Load(Tokenizer &token);
    void preComputeTangent();
    void preComputeCoeff();
};


#endif