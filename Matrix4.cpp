#include <math.h>

#include "Matrix4.h"

Matrix4::Matrix4()
{
    for (int i=0; i<4; ++i)
    {
        for (int j=0; j<4; ++j)
        {
            m[i][j] = 0;
        }
    }
    //v4 = Vector4(0.0, 0.0, 0.0, 0.0);
}

Matrix4& Matrix4::operator=(const Matrix4& m2)
{
    if (this != &m2)
    {
        for (int i=0; i<4; ++i)
        {
            for (int j=0; j<4; ++j)
            {
                m[i][j] = m2.m[i][j];
            }
        }
    }
    return *this;
}

Matrix4& Matrix4::operator+(const Matrix4& m2)
{
    for(int i = 0; i < 4; i++)
    {
        for(int j = 0; j < 4; j++)
        {
            m[i][j] += m2.m[i][j];
        }
    }
    return *this;
}

Matrix4& Matrix4::operator*(const Matrix4& m2)
{
	Matrix4 matrix;
	for (int c = 0; c < 4; c++)
	{
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				matrix.m[i][c] += this->m[i][j] * m2.m[j][c];
			}
		}
	}
    
    *this = matrix;
	return *this;
}

Vector4& Matrix4::operator*(const Vector4& v)
{
    Vector4 v1 = Vector4(0.0, 0.0, 0.0, 0.0);
    for(int i = 0; i < 4; i++)
    {
        for(int j = 0; j < 4; j++)
        {
            v1.vector4[i] += m[i][j] * v.vector4[j];
        }
    }
    v4.setValues(v1.getX(), v1.getY(), v1.getZ(), v1.getW());
    //v4.print("v4:");
    return this->v4;
}

// return pointer to matrix elements
double* Matrix4::getPointer()
{
    return &m[0][0];
}

// set matrix to identity matrix
void Matrix4::identity()
{
    for (int i=0; i<4; ++i)
    {
        for (int j=0; j<4; ++j)
        {
            if (i==j)
                m[i][j] = 1.0;
            else
                m[i][j] = 0.0;
        }
    }
}

// transpose the matrix (mirror at diagonal)
void Matrix4::transpose()
{
    Matrix4 temp;
    for (int i=0; i<4; ++i)
    {
        for (int j=0; j<4; ++j)
        {
            temp.m[j][i] = m[i][j];
        }
    }
    *this = temp;  // copy temporary values to this matrix
    //this->print("");
}

// Creates a rotation matrix which rotates about the y axis.
// angle is expected in degrees
void Matrix4::makeRotateY(double angle)
{
    angle = angle / 180.0 * M_PI;  // convert from degrees to radians
    identity();
    m[0][0] = cos(angle);
    m[0][2] = sin(angle);
    m[2][0] = -sin(angle);
    m[2][2] = cos(angle);
}

void Matrix4::makeRotateX(double angle)
{
    angle = angle / 180.0 * M_PI;  // convert from degrees to radians
    identity();
	m[1][1] = cos(angle);
	m[1][2] = -sin(angle);
	m[2][1] = sin(angle);
	m[2][2] = cos(angle);
}

void Matrix4::makeRotateZ(double angle)
{
    angle = angle / 180.0 * M_PI;  // convert from degrees to radians
    identity();
	m[0][0] = cos(angle);
	m[0][1] = -sin(angle);
	m[1][0] = sin(angle);
	m[1][1] = cos(angle);
}

void Matrix4::makeRotate(double angle, V3& axis)
{
    V3 a = V3(axis.getX(), axis.getY(), axis.getZ());
    a.normalize();
    //angle = angle / 180.0 * M_PI;
    identity();
    m[0][0] = a.getX() * a.getX() + (a.getY() * a.getY() + a.getZ() * a.getZ()) * cos(angle);
    m[0][1] = (a.getX() * a.getY()) * (1-cos(angle)) - a.getZ() * sin(angle);
    m[0][2] = (a.getX() * a.getZ()) * (1-cos(angle)) + a.getY() * sin(angle);
    m[1][0] = (a.getX() * a.getY()) * (1-cos(angle)) + a.getZ() * sin(angle);
    m[1][1] = a.getY() * a.getY() + (a.getX() * a.getX() + a.getZ() * a.getZ()) * cos(angle);
    m[1][2] = (a.getY() * a.getZ()) * (1-cos(angle)) - a.getX() * sin(angle);
    m[2][0] = (a.getX() * a.getZ()) * (1-cos(angle)) - a.getY() * sin(angle);
    m[2][1] = (a.getY() * a.getZ()) * (1-cos(angle)) + a.getX() * sin(angle);
    m[2][2] = a.getZ() * a.getZ() + (a.getX() * a.getX() + a.getY() * a.getY()) * cos(angle);
}

void Matrix4::makeTranslate(double tx, double ty, double tz)
{
    identity();
	m[0][3] = tx;
	m[1][3] = ty;
	m[2][3] = tz;
}

void Matrix4::makeScale(double sx, double sy, double sz)
{
    identity();
    m[0][0] = sx;
    m[1][1] = sy;
    m[2][2] = sz;
}

void Matrix4::setX(V3 x)
{
	m[0][0] = x.getX();
	m[1][0] = x.getY();
	m[2][0] = x.getZ();
}

void Matrix4::setY(V3 y)
{
	m[0][1] = y.getX();
	m[1][1] = y.getY();
	m[2][1] = y.getZ();
}

void Matrix4::setZ(V3 z)
{
	m[0][2] = z.getX();
	m[1][2] = z.getY();
	m[2][2] = z.getZ();
}

void Matrix4::setX_W(Vector4 x)
{
    m[0][0] = x.getX();
    m[1][0] = x.getY();
    m[2][0] = x.getZ();
    m[3][0] = x.getW();
}

void Matrix4::setY_W(Vector4 y)
{
    m[0][1] = y.getX();
    m[1][1] = y.getY();
    m[2][1] = y.getZ();
    m[3][1] = y.getW();
}

void Matrix4::setZ_W(Vector4 z)
{
    m[0][2] = z.getX();
    m[1][2] = z.getY();
    m[2][2] = z.getZ();
    m[3][2] = z.getW();
}

void Matrix4::setW_W(Vector4 w)
{
    m[0][3] = w.getX();
    m[1][3] = w.getY();
    m[2][3] = w.getZ();
    m[3][3] = w.getW();
}

void Matrix4::scaleAll(double scale)
{
    m[0][0] = scale * m[0][0];
    m[0][1] = scale * m[0][1];
    m[0][2] = scale * m[0][2];
    m[0][3] = scale * m[0][3];
    m[1][0] = scale * m[1][0];
    m[1][1] = scale * m[1][1];
    m[1][2] = scale * m[1][2];
    m[1][3] = scale * m[1][3];
    m[2][0] = scale * m[2][0];
    m[2][1] = scale * m[2][1];
    m[2][2] = scale * m[2][2];
    m[2][3] = scale * m[2][3];
}

void Matrix4::inverse()
{
    double Result[4][4];
    double tmp[12]; /* temp array for pairs */
    double src[16]; /* array of transpose source matrix */
    double det; /* determinant */
    /* transpose matrix */
    for (int i = 0; i < 4; i++)
    {
        src[i + 0 ] = m[i][0];
        src[i + 4 ] = m[i][1];
        src[i + 8 ] = m[i][2];
        src[i + 12] = m[i][3];
    }
    /* calculate pairs for first 8 elements (cofactors) */
    tmp[0] = src[10] * src[15];
    tmp[1] = src[11] * src[14];
    tmp[2] = src[9] * src[15];
    tmp[3] = src[11] * src[13];
    tmp[4] = src[9] * src[14];
    tmp[5] = src[10] * src[13];
    tmp[6] = src[8] * src[15];
    tmp[7] = src[11] * src[12];
    tmp[8] = src[8] * src[14];
    tmp[9] = src[10] * src[12];
    tmp[10] = src[8] * src[13];
    tmp[11] = src[9] * src[12];
    /* calculate first 8 elements (cofactors) */
    Result[0][0] = tmp[0]*src[5] + tmp[3]*src[6] + tmp[4]*src[7];
    Result[0][0] -= tmp[1]*src[5] + tmp[2]*src[6] + tmp[5]*src[7];
    Result[0][1] = tmp[1]*src[4] + tmp[6]*src[6] + tmp[9]*src[7];
    Result[0][1] -= tmp[0]*src[4] + tmp[7]*src[6] + tmp[8]*src[7];
    Result[0][2] = tmp[2]*src[4] + tmp[7]*src[5] + tmp[10]*src[7];
    Result[0][2] -= tmp[3]*src[4] + tmp[6]*src[5] + tmp[11]*src[7];
    Result[0][3] = tmp[5]*src[4] + tmp[8]*src[5] + tmp[11]*src[6];
    Result[0][3] -= tmp[4]*src[4] + tmp[9]*src[5] + tmp[10]*src[6];
    Result[1][0] = tmp[1]*src[1] + tmp[2]*src[2] + tmp[5]*src[3];
    Result[1][0] -= tmp[0]*src[1] + tmp[3]*src[2] + tmp[4]*src[3];
    Result[1][1] = tmp[0]*src[0] + tmp[7]*src[2] + tmp[8]*src[3];
    Result[1][1] -= tmp[1]*src[0] + tmp[6]*src[2] + tmp[9]*src[3];
    Result[1][2] = tmp[3]*src[0] + tmp[6]*src[1] + tmp[11]*src[3];
    Result[1][2] -= tmp[2]*src[0] + tmp[7]*src[1] + tmp[10]*src[3];
    Result[1][3] = tmp[4]*src[0] + tmp[9]*src[1] + tmp[10]*src[2];
    Result[1][3] -= tmp[5]*src[0] + tmp[8]*src[1] + tmp[11]*src[2];
    /* calculate pairs for second 8 elements (cofactors) */
    tmp[0] = src[2]*src[7];
    tmp[1] = src[3]*src[6];
    tmp[2] = src[1]*src[7];
    tmp[3] = src[3]*src[5];
    tmp[4] = src[1]*src[6];
    tmp[5] = src[2]*src[5];
    
    tmp[6] = src[0]*src[7];
    tmp[7] = src[3]*src[4];
    tmp[8] = src[0]*src[6];
    tmp[9] = src[2]*src[4];
    tmp[10] = src[0]*src[5];
    tmp[11] = src[1]*src[4];
    /* calculate second 8 elements (cofactors) */
    Result[2][0] = tmp[0]*src[13] + tmp[3]*src[14] + tmp[4]*src[15];
    Result[2][0] -= tmp[1]*src[13] + tmp[2]*src[14] + tmp[5]*src[15];
    Result[2][1] = tmp[1]*src[12] + tmp[6]*src[14] + tmp[9]*src[15];
    Result[2][1] -= tmp[0]*src[12] + tmp[7]*src[14] + tmp[8]*src[15];
    Result[2][2] = tmp[2]*src[12] + tmp[7]*src[13] + tmp[10]*src[15];
    Result[2][2] -= tmp[3]*src[12] + tmp[6]*src[13] + tmp[11]*src[15];
    Result[2][3] = tmp[5]*src[12] + tmp[8]*src[13] + tmp[11]*src[14];
    Result[2][3] -= tmp[4]*src[12] + tmp[9]*src[13] + tmp[10]*src[14];
    Result[3][0] = tmp[2]*src[10] + tmp[5]*src[11] + tmp[1]*src[9];
    Result[3][0] -= tmp[4]*src[11] + tmp[0]*src[9] + tmp[3]*src[10];
    Result[3][1] = tmp[8]*src[11] + tmp[0]*src[8] + tmp[7]*src[10];
    Result[3][1] -= tmp[6]*src[10] + tmp[9]*src[11] + tmp[1]*src[8];
    Result[3][2] = tmp[6]*src[9] + tmp[11]*src[11] + tmp[3]*src[8];
    Result[3][2] -= tmp[10]*src[11] + tmp[2]*src[8] + tmp[7]*src[9];
    Result[3][3] = tmp[10]*src[10] + tmp[4]*src[8] + tmp[9]*src[9];
    Result[3][3] -= tmp[8]*src[9] + tmp[11]*src[10] + tmp[5]*src[8];
    /* calculate determinant */
    det=src[0]*Result[0][0]+src[1]*Result[0][1]+src[2]*Result[0][2]+src[3]*Result[0][3];
    /* calculate matrix inverse */
    det = 1.0f / det;
    
    //Matrix4 FloatResult;
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            m[i][j] = float(Result[i][j] * det);
        }
    }
}

void Matrix4::print(string comment)
{
    cout << comment << "\n";
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            if (j != 3) cout << m[i][j] << "\t";
            else cout << m[i][j] << endl;;
        }
    }
}