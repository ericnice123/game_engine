#ifndef SPRINGDAMPER_H
#define SPRINGDAMPER_H

#include "Particle.h"
#include <GLUT/glut.h>

class SpringDamper
{
private:
    float SpringConstant, DampingFactor;
    float RestLength;
    Particle *P1, *P2;
    
public:
    SpringDamper(Particle *p1, Particle *p2);
    void ComputeForce();
    void Draw();
};

#endif