#include "Skeleton.h"

bool Skeleton::load(const char *file)
{
    numofjoints = 1;
    Tokenizer token;
    token.Open(file);
    token.FindToken("balljoint");
    // Parse tree
    char name[256];
    token.GetToken(name);
    root=new Joint(name, numofjoints);
    joint.push_back(root);
    root->load(token, joint);
    
    numofjoints--;
    // Finish
    token.Close();
    return true;
}

void Skeleton::update(Matrix4 world)//update the root
{
    worldMatrix.clear();
    Matrix4 copy_world = world;
    root->update(copy_world, worldMatrix);
}

void Skeleton::draw()
{
    root->draw();
}

void Skeleton::print()
{
    root->print();
}

Matrix4 Skeleton::getWorldMatrix(int i)
{
    return worldMatrix[i];
}
