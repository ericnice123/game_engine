//
//  Vector4.h
//  Project_1
//
//  Created by Tsung-Huan Tsai on 10/6/14.
//  Copyright (c) 2014 Tsung-Huan Tsai. All rights reserved.
//

#ifndef _VECTOR4_H
#define _VECTOR4_H

#include <string>
#include <iostream>
using namespace std;

class Vector4
{
protected:
    
public:
    double vector4[4];
    Vector4(double x, double y, double z, double w); //constructor for x y and z
    Vector4& operator+(const Vector4&); //overload operator +
    Vector4& operator-(const Vector4&); //overload operator -
    //Vector4& operator=(const Vector4&);
	void dehomogenize(); //dehomogenize the vector (scale it so that its fourth component is equal to one)
	void print(string comment); //print x y z and w components
    double getX();
    double getY();
    double getZ();
    double getW();
    void setW(double);
    double getElement(int i);
    void setValues(double x, double y, double z, double w);
};

#endif