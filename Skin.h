#ifndef SKIN_H
#define SKIN_H

#include "Triangle.h"
#include "Vertex.h"
#include "token.h"
#include "skinWeight.h"
#include "Skeleton.h"
#include "V3.h"
#include "Vector4.h"
#include <vector>

using namespace std;

class Skin
{
    vector<Vertex> positions;
    vector<Vertex> normals;
    vector<Triangle> triangles;
    vector<SkinWeight> skinweights;
    vector<Matrix4> bindings_inverse;
    Skeleton* skeleton;
    
    //v' and n'
    vector<Vertex> positions_prime;
    vector<Vertex> normals_prime;
    
public:
    bool load(const char *file, Skeleton* skel);
    void update();
    void draw();
    void drawNormals();
    void loadPositions(Tokenizer &token);
    void loadNormals(Tokenizer &token);
    void loadTriangles(Tokenizer &token);
    void loadBindings(Tokenizer &token);
    void loadSkinWeights(Tokenizer &token);
    void computeV_prime();
    void computeN_prime();
};

#endif