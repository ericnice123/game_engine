#ifndef ANIMATIONCLIP_H
#define ANIMATIONCLIP_H


#include <vector>
#include "Skeleton.h"
#include "Channel.h"
#include "token.h"

using namespace std;

class AnimationClip
{
private:
    float timeStart, timeEnd;
    int numOfChannels;
    vector<Channel> channels;
    Skeleton* skeleton;
    
public:
    void Evaluate(float time);
    bool load(const char *filename, Skeleton *skeleton);
};


#endif