#ifndef AERODYNAMIC_H
#define AERODYNAMIC_H

#include "Particle.h"
#include "V3.h"

class AeroDynamic
{
    float density = 1.3; // constant
    float Cd = 0.3; // constant
    float a; // area
    Particle *P1, *P2, *P3; // three particle makes a triangle
    V3 Vair = V3(0,0, -50);
    bool firstTriangle;
public:
    AeroDynamic(Particle *p1, Particle *p2, Particle *p3, bool firstT);
    void ComputeForce();
    void setVair(V3 &vair);
    V3& getVair();
};

#endif