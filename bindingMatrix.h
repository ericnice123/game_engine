#ifndef BINDINGMATRIX_H
#define BINDINGMATRIX_H

#include "Matrix4.h"
#include "token.h"
#include <vector>
#include <iostream>
#include "V3.h"
#include "Vector4.h"

using namespace std;

class BindingMatrix
{
public:
    Matrix4 bindings_inverse;
};

#endif