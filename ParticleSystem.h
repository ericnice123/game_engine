#ifndef PARTICLESYSTEM_H
#define PARTICLESYSTEM_H

#include "Particle.h"
#include "SpringDamper.h"
#include "AeroDynamic.h"
#include <GLUT/glut.h>
#include <vector>

using namespace std;

class ParticleSystem
{
private:
    int NumParticles;
    int row;
    int column;
    vector<vector<Particle*> > P;
    vector<SpringDamper*> SP;
    
public:
    vector<Particle*> staticP;
    vector<AeroDynamic*> AD;
    void Update(float deltaTime);
    void Draw();
    void Build(int NumParticles, int row, int column);
    void BuildSpringDamper(Particle* p1, Particle* p2);
    void BuildAeroDynamic();
    void DrawDebug();
    void computeNormals();
};

#endif