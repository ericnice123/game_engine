#include "AnimationClip.h"


bool AnimationClip::load(const char *filename, Skeleton *skeleton)
{
    this->skeleton = skeleton;
    Tokenizer token;
    token.Open(filename);
    
    // getting the timeStart and End
    token.FindToken("range");
    timeStart = token.GetFloat();
    timeEnd = token.GetFloat();
    //cout << "range " << timeStart << " " << timeEnd << endl;
    
    // getting the size of channels
    token.FindToken("numchannels");
    numOfChannels = token.GetInt();
    //cout << "numchannels " << numOfChannels << endl;
    
    // loading all the channels
    for(int i = 0; i < numOfChannels; i++)
    {
        Channel channel;
        channel.Load(token);
        channel.preComputeTangent();
        channel.preComputeCoeff();
        channels.push_back(channel);
    }
    
    token.Close();
    return true;
}

void AnimationClip::Evaluate(float time)
{
    int which_angle = 0;
    int jointIndex = 0;
    for(int i = 0; i < channels.size(); i++)
    {
        // translation part
        if(i == 0)
        {
            skeleton->joint[0]->offsetx = channels[i].Evaluate(time);
        }
        else if(i == 1)
        {
            skeleton->joint[0]->offsety = channels[i].Evaluate(time);
        }
        else if(i == 2)
        {
            skeleton->joint[0]->offsetz = channels[i].Evaluate(time);
        }
        else if(i > 2)
        {
            if(which_angle == 0)
            {
                skeleton->joint[jointIndex]->posex = channels[i].Evaluate(time);
                which_angle++;
            }
            else if(which_angle == 1)
            {
                skeleton->joint[jointIndex]->posey = channels[i].Evaluate(time);
                which_angle++;
            }
            else if(which_angle == 2)
            {
                skeleton->joint[jointIndex]->posez = channels[i].Evaluate(time);
                which_angle = 0;
                jointIndex++;
            }
        }
    }
}