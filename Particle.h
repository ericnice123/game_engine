#ifndef PARTICLE_H
#define PARTICLE_H

#include "V3.h"
#include <GLUT/glut.h>

class Particle
{
public:
    float Mass;
    float v_x, v_y, v_z = 0;
    float elasticity = 0.1;
    float friction = 0.4;
    V3 Position = V3(0,0,0);
    V3 Velocity = V3(0,0,0);
    V3 Force = V3(0,0,0);
    V3 normal = V3(0,0,0);
    
    void Update(float deltaTime);
    void Draw();
    void ApplyForce(V3 &f) {Force + f;}
    void Build(V3 initPos, float mass);
    void SetPosition(V3 &pos);
};

#endif