#ifndef ANIMATIONPLAYER_H
#define ANIMATIONPLAYER_H

#include "AnimcationClip.h"

class AnimationPlayer
{
private:
    float Time;
    AnimationClip *Anim;
    //Pose p;
public:
    void SetClip(AnimationClip &clip);
    //const Pose &GetPose();
    void Update();
};

#endif