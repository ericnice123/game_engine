#ifndef _MATRIX4_H_
#define _MATRIX4_H_

//#define M_PI 3.14159265358979323846 //Pi, could uncomment this if machine not finding this
#include "Vector4.h"
#include "V3.h"

class Matrix4
{
  protected:
    double m[4][4];   // matrix elements; first index is for rows, second for columns (row-major)
    Matrix4* multipleMatrix;
    Vector4 v4 = Vector4(0.0, 0.0, 0.0, 0.0);
    
  public:
    Matrix4();     
    Matrix4& operator=(const Matrix4&);
    Matrix4& operator+(const Matrix4&);
    double* getPointer(); 
    void identity(); 
    void transpose();
    void inverse();
    void makeRotateY(double);
    void makeRotateX(double);
    void makeRotateZ(double);
    Matrix4& operator*(const Matrix4& m2);
    Vector4& operator*(const Vector4& v);
    void makeRotate(double angle, V3& axis);
    void makeScale(double sx, double sy, double sz);
    void makeTranslate(double tx, double ty, double tz);
    void print(string comment);
	void setX(V3 x); // set the X-axis
	void setY(V3 y); // set teh Y-axis
	void setZ(V3 z); // set teh Z-axis
    void setX_W(Vector4 x);
    void setY_W(Vector4 y);
    void setZ_W(Vector4 z);
    void setW_W(Vector4 w);
    void scaleAll(double scale);
};

#endif