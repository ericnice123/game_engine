#ifndef KEYFRAME_H
#define KEYFRAME_H

class Keyframe
{
public:
    float Time;
    float Value;
    float TangentIn, TangentOut;
    char RuleIn, RuleOut;
    float a,b,c,d;
};

#endif