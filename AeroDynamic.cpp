#include "AeroDynamic.h"

AeroDynamic::AeroDynamic(Particle *p1, Particle *p2, Particle *p3, bool firstT)
{
    P1 = p1;
    P2 = p2;
    P3 = p3;
    firstTriangle = firstT;
}


void AeroDynamic::ComputeForce()
{
    // Vsurface = (v1+v2+v3)/3
    V3 v1 = V3(P1->Velocity.getX(), P1->Velocity.getY(), P1->Velocity.getZ());
    V3 v2 = V3(P2->Velocity.getX(), P2->Velocity.getY(), P2->Velocity.getZ());
    V3 v3 = V3(P3->Velocity.getX(), P3->Velocity.getY(), P3->Velocity.getZ());
    V3 Vsurface = v1+ v2 + v3;
    Vsurface.scale(1.0/3.0);
    
    // V = Vsurface - Vair
    V3 V = Vsurface - Vair;
    
    // normal (r1 - r2) x (r3 -r2) and normalize
    V3 r1 = V3(P1->Position.getX(), P1->Position.getY(), P1->Position.getZ());
    V3 r2 = V3(P2->Position.getX(), P2->Position.getY(), P2->Position.getZ());
    V3 r3 = V3(P3->Position.getX(), P3->Position.getY(), P3->Position.getZ());
    V3 r1_r2 = r1 - r2;
    V3 r3_r2 = r3 - r2;
    V3 n = V3(0,0,0);
    if(firstTriangle) n.cross(r3_r2, r1_r2);
    else n.cross(r1_r2, r3_r2);
    
    float a0 = 0.5 * n.length(); // a0
    n.normalize();
    
    // a
    float V_length = V.length();
    float v_dot_n = V.dot(V, n);
    float a = a0 * (v_dot_n / V_length);
    
    // |V|^2
    float V_square = V_length * V_length;
    
    // all scaler
    float s = -0.5 * density * V_square * Cd * a;
    n.scale(s);
    
    n.scale(1.0/3.0);
    P1->ApplyForce(n);
    P2->ApplyForce(n);
    P3->ApplyForce(n);
}

void AeroDynamic::setVair(V3 & vair)
{
    this->Vair = vair;
}

V3& AeroDynamic::getVair()
{
    return this->Vair;
}