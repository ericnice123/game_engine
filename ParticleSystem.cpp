#include "ParticleSystem.h"

void ParticleSystem::Update(float deltaTime)
{
    // compute forces
    V3 gravity(0,-9.8,0);
    bool twoCorner = false;
    for(int r = 0; r < row; r++)
    {
        for(int c = 0; c < column; c++)
        {
            if((r == 0 && c == 0) || (r == 0 && c == column - 1)) twoCorner = true;
            else twoCorner = false;
            
            if(!twoCorner)
            {
                V3 copy_gravity = V3(gravity.getX(), gravity.getY(), gravity.getZ());
                copy_gravity.scale(P[r][c]->Mass); // f = ma
                V3 force = V3(copy_gravity.getX(), copy_gravity.getY(), copy_gravity.getZ());
                P[r][c]->ApplyForce(force);
            }
        }
    }
    
    
    // compute springdamper
    for(int i = 0; i < SP.size(); i++)
    {
        SP[i]->ComputeForce();
    }
    
    // compute Aero
    for(int i = 0; i < AD.size(); i++)
    {
        AD[i]->ComputeForce();
    }
    
    // integrate
    for(int r = 0; r < row; r++)
    {
        for(int c = 0; c < column; c++)
        {
            if(!((r == 0 && c == 0) || (r == 0 && c == column - 1))) P[r][c]->Update(deltaTime);
        }
    }
}

void ParticleSystem::Draw()
{
    //computeNormals();
    glBegin(GL_TRIANGLES);
    for(int r = 0; r < row - 1; r++)
    {
        for(int c = 0; c < column - 1; c++)
        {
            V3 r1 = V3(P[r][c]->Position.getX(), P[r][c]->Position.getY(), P[r][c]->Position.getZ());
            V3 r2 = V3(P[r+1][c]->Position.getX(), P[r+1][c]->Position.getY(), P[r+1][c]->Position.getZ());
            V3 r3 = V3(P[r+1][c+1]->Position.getX(), P[r+1][c+1]->Position.getY(), P[r+1][c+1]->Position.getZ());
            V3 r1_r2 = r1 - r2;
            V3 r3_r2 = r3 - r2;
            V3 n = V3(0,0,0);
            n.cross(r3_r2, r1_r2);
            n.normalize();
            //n.print("n1");
            glNormal3d(n.getX(), n.getY(), n.getZ());
            
            /*glNormal3d(P[r][c]->normal.getX(), P[r][c]->normal.getY(), P[r][c]->normal.getZ());
            glNormal3d(P[r+1][c]->normal.getX(), P[r+1][c]->normal.getY(), P[r+1][c]->normal.getZ());
            glNormal3d(P[r+1][c+1]->normal.getX(), P[r+1][c+1]->normal.getY(), P[r+1][c+1]->normal.getZ());*/
            glVertex3d(P[r][c]->Position.getX(), P[r][c]->Position.getY(), P[r][c]->Position.getZ());
            glVertex3d(P[r+1][c]->Position.getX(), P[r+1][c]->Position.getY(), P[r+1][c]->Position.getZ());
            glVertex3d(P[r+1][c+1]->Position.getX(), P[r+1][c+1]->Position.getY(), P[r+1][c+1]->Position.getZ());
            
            
            r1 = V3(P[r][c]->Position.getX(), P[r][c]->Position.getY(), P[r][c]->Position.getZ());
            r2 = V3(P[r][c+1]->Position.getX(), P[r][c+1]->Position.getY(), P[r][c+1]->Position.getZ());
            r3 = V3(P[r+1][c+1]->Position.getX(), P[r+1][c+1]->Position.getY(), P[r+1][c+1]->Position.getZ());
            r1_r2 = r1 - r2;
            r3_r2 = r3 - r2;
            n = V3(0,0,0);
            n.cross(r1_r2, r3_r2);
            n.normalize();
            //n.print("n2");
            glNormal3d(n.getX(), n.getY(), n.getZ());
            
            /*glNormal3d(P[r][c]->normal.getX(), P[r][c]->normal.getY(), P[r][c]->normal.getZ());
            glNormal3d(P[r+1][c+1]->normal.getX(), P[r+1][c+1]->normal.getY(), P[r+1][c+1]->normal.getZ());
            glNormal3d(P[r+1][c]->normal.getX(), P[r+1][c]->normal.getY(), P[r+1][c]->normal.getZ());*/
            glVertex3d(P[r][c]->Position.getX(), P[r][c]->Position.getY(), P[r][c]->Position.getZ());
            glVertex3d(P[r][c+1]->Position.getX(), P[r][c+1]->Position.getY(), P[r][c+1]->Position.getZ());
            glVertex3d(P[r+1][c+1]->Position.getX(), P[r+1][c+1]->Position.getY(), P[r+1][c+1]->Position.getZ());
        }
    }
    glEnd();
    
    
}

void ParticleSystem::Build(int numParticles, int row, int column)
{
    if(row * column != numParticles)
    {
        cout << "row * column != numParticles" << endl;
        exit(0);
    }
    
    P = vector<vector<Particle*> >(row);
    
    // init all the instances
    NumParticles = numParticles;
    this->row = row;
    this->column = column;
    
    double y_offset = 5.0/(double)row;
    double x_offset = 5.0/(double)column;
    V3 particlePosition = V3(-2.5,5.0,0);
    
    // build the particles based on passed in parameters
    for(int r = 0; r < row; r++)
    {
        // setting the y as row increment
        
        for(int c = 0; c < column; c++)
        {
            Particle *p = new Particle();
            //cout << particlePosition.getX() << " " << particlePosition.getY() << " " << particlePosition.getZ() << endl;
            p->Build(particlePosition, 1.0);
            P[r].push_back(p);
            if((r == 0 && c == 0) || (r == 0 && c == column -1)) staticP.push_back(p);
            particlePosition.setX(particlePosition.getX() + x_offset);
        }
        
        // setting back the x as row increment
        particlePosition.setY(particlePosition.getY() - y_offset);
        particlePosition.setX(-2.5);
    }

    // building SpringDamper
    for(int r = 0; r < row; r++)
    {
        bool leftEdge = false;
        bool rightEdge = false;
        bool bottomLine = false;
        bool lastOne = false;
        for(int c = 0; c < column; c++)
        {
            if(r == row -1 && c == column -1) lastOne = true;
            if(c == 0 && r != row -1) leftEdge = true;
            if(r == row -1 && c != column - 1) bottomLine = true;
            if(c == column - 1 && r != row -1) rightEdge = true;
            // case 1
            if(leftEdge)
            {
                BuildSpringDamper(P[r][c], P[r+1][c]);
                BuildSpringDamper(P[r][c], P[r+1][c+1]);
                BuildSpringDamper(P[r][c], P[r][c+1]);
            }
            // case 2
            else if(rightEdge)
            {
                BuildSpringDamper(P[r][c], P[r+1][c-1]);
                BuildSpringDamper(P[r][c], P[r+1][c]);
            }
            // case 3
            else if(bottomLine)
            {
                BuildSpringDamper(P[r][c], P[r][c+1]);
            }
            // case 4
            else if(lastOne)
            {
                // do nothing
            }
            // case 5
            else
            {
                BuildSpringDamper(P[r][c], P[r+1][c-1]);
                BuildSpringDamper(P[r][c], P[r+1][c]);
                BuildSpringDamper(P[r][c], P[r+1][c+1]);
                BuildSpringDamper(P[r][c], P[r][c+1]);
            }
            leftEdge = false;
            rightEdge = false;
            bottomLine = false;
            lastOne = false;
        }
    }
    
    // build AeroDynamic
    BuildAeroDynamic();
}

void ParticleSystem::BuildSpringDamper(Particle *p1, Particle *p2)
{
    SpringDamper* springDamper = new SpringDamper(p1, p2);
    this->SP.push_back(springDamper);
}

void ParticleSystem::BuildAeroDynamic()
{
    for(int r = 0; r < row - 1; r++)
    {
        for(int c = 0; c < column - 1; c++)
        {
            AeroDynamic* ad1 = new AeroDynamic(P[r][c], P[r+1][c], P[r+1][c+1], true);
            AeroDynamic* ad2 = new AeroDynamic(P[r][c], P[r][c+1], P[r+1][c+1], false);
            AD.push_back(ad1);
            AD.push_back(ad2);
        }
    }
}

void ParticleSystem::DrawDebug()
{
    for(int i = 0; i < SP.size(); i++)
    {
        SP[i]->Draw();
    }
}

void ParticleSystem::computeNormals()
{
    for(int r = 0; r < row; r++)
    {
        for(int c = 0; c < column; c++)
        {
            P[r][c]->normal.setX(0);
            P[r][c]->normal.setY(0);
            P[r][c]->normal.setZ(0);
        }
    }
    
    // for each triangle compute normal and add them
    for(int r = 0; r < row -1; r++)
    {
        for(int c = 0; c < column - 1; c++)
        {
            // first triangle
            V3 p1 = V3(P[r][c]->Position.getX(), P[r][c]->Position.getY(), P[r][c]->Position.getZ());
            V3 p2 = V3(P[r+1][c]->Position.getX(), P[r+1][c]->Position.getY(), P[r+1][c]->Position.getZ());
            V3 p3 = V3(P[r+1][c+1]->Position.getX(), P[r+1][c+1]->Position.getY(), P[r+1][c+1]->Position.getZ());
            V3 p3_p1 = p3 - p1;
            V3 p2_p1 = p2 - p1;
            V3 n = V3(0,0,0);
            n.cross(p2_p1, p3_p1);
            //n.print("gonichiwa");
            P[r][c]->normal = P[r][c]->normal + n;
            //P[r][c]->normal.print("n1");
            
            p1 = V3(P[r][c]->Position.getX(), P[r][c]->Position.getY(), P[r][c]->Position.getZ());
            p2 = V3(P[r+1][c]->Position.getX(), P[r+1][c]->Position.getY(), P[r+1][c]->Position.getZ());
            p3 = V3(P[r+1][c+1]->Position.getX(), P[r+1][c+1]->Position.getY(), P[r+1][c+1]->Position.getZ());
            V3 p3_p2 = p3 - p2;
            V3 p1_p2 = p1 - p2;
            n = V3(0,0,0);
            n.cross(p3_p2, p1_p2);
            P[r+1][c]->normal = P[r+1][c]->normal + n;
            //P[r+1][c]->normal.print("n2");
            
            p1 = V3(P[r][c]->Position.getX(), P[r][c]->Position.getY(), P[r][c]->Position.getZ());
            p2 = V3(P[r+1][c]->Position.getX(), P[r+1][c]->Position.getY(), P[r+1][c]->Position.getZ());
            p3 = V3(P[r+1][c+1]->Position.getX(), P[r+1][c+1]->Position.getY(), P[r+1][c+1]->Position.getZ());
            V3 p1_p3 = p1 - p3;
            V3 p2_p3 = p2 - p3;
            n = V3(0,0,0);
            n.cross(p1_p3, p2_p3);
            P[r+1][c+1]->normal = P[r+1][c+1]->normal + n;
            //P[r+1][c+1]->normal.print("n3");
            
            // second triangle
            p1 = V3(P[r][c]->Position.getX(), P[r][c]->Position.getY(), P[r][c]->Position.getZ());
            p2 = V3(P[r+1][c+1]->Position.getX(), P[r+1][c+1]->Position.getY(), P[r+1][c+1]->Position.getZ());
            p3 = V3(P[r][c+1]->Position.getX(), P[r][c+1]->Position.getY(), P[r][c+1]->Position.getZ());
            p2_p1 = V3(0,0,0);
            p3_p1 = V3(0,0,0);
            p2_p1 = p2 - p1;
            p3_p1 = p3 - p1;
            n = V3(0,0,0);
            n.cross(p2_p1, p3_p1);
            P[r][c]->normal = P[r][c]->normal + n;
            //P[r][c]->normal.print("n1");
            
            p1 = V3(P[r][c]->Position.getX(), P[r][c]->Position.getY(), P[r][c]->Position.getZ());
            p2 = V3(P[r+1][c+1]->Position.getX(), P[r+1][c+1]->Position.getY(), P[r+1][c+1]->Position.getZ());
            p3 = V3(P[r][c+1]->Position.getX(), P[r][c+1]->Position.getY(), P[r][c+1]->Position.getZ());
            p3_p2 = V3(0,0,0);
            p1_p2 = V3(0,0,0);
            p3_p2 = p3 - p2;
            p1_p2 = p1 - p2;
            n = V3(0,0,0);
            n.cross(p3_p2, p1_p2);
            P[r+1][c+1]->normal = P[r+1][c+1]->normal + n;
            
            p1 = V3(P[r][c]->Position.getX(), P[r][c]->Position.getY(), P[r][c]->Position.getZ());
            p2 = V3(P[r+1][c+1]->Position.getX(), P[r+1][c+1]->Position.getY(), P[r+1][c+1]->Position.getZ());
            p3 = V3(P[r][c+1]->Position.getX(), P[r][c+1]->Position.getY(), P[r][c+1]->Position.getZ());
            p1_p3 = V3(0,0,0);
            p2_p3 = V3(0,0,0);
            p1_p3 = p1 - p3;
            p2_p3 = p2 - p3;
            n = V3(0,0,0);
            n.cross(p1_p3, p2_p3);
            P[r][c+1]->normal = P[r][c+1]->normal + n;
        }
    }
    
    
    // for each particle, normalize
    for(int r = 0; r < row; r++)
    {
        for(int c = 0; c < column; c++)
        {
            P[r][c]->normal.normalize();
        }
    }
}




