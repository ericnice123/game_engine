#include "Vector4.h"

Vector4::Vector4(double x, double y, double z, double w)
{
    vector4[0] = x;
    vector4[1] = y;
    vector4[2] = z;
    vector4[3] = w;
}

Vector4& Vector4::operator+(const Vector4& v2)
{
    for (int i = 0; i < 4; i++)
        vector4[i] = vector4[i] + v2.vector4[i];
    return *this;
}

/*Vector4& Vector4::operator=(const Vector4& v2)
{
    for (int i = 0; i < 4; i++)
        vector4[i] = v2.vector4[i];
    return *this;
}*/

Vector4& Vector4::operator-(const Vector4& v2)
{
    for (int i = 0; i < 4; i++)
        vector4[i] = vector4[i] - v2.vector4[i];
    return *this;
}

void Vector4::dehomogenize()//make sure the buttom right element is 1
{
    if (vector4[3] == 0) cout << "can't divided by 0" << endl;
    else
    {
        for (int i = 0; i < 4; i++)
        {
            vector4[i] = vector4[i] / vector4[3];
        }
    }
}

double Vector4::getX()
{
    return vector4[0];
}

double Vector4::getY()
{
    return vector4[1];
}

double Vector4::getZ()
{
    return vector4[2];
}

double Vector4::getW()
{
    return vector4[3];
}

double Vector4::getElement(int i)
{
    return vector4[i];
}

void Vector4::setValues(double x, double y, double z, double w)
{
    vector4[0] = x;
    vector4[1] = y;
    vector4[2] = z;
    vector4[3] = w;
}

void Vector4::setW(double value)
{
    vector4[3] = value;
}

void Vector4::print(string comment)
{
    cout << comment << " " << vector4[0] << " " << vector4[1] << " " << vector4[2] << " " << vector4[3] << endl;
}