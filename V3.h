//
//  V3.h
//  Project_1
//
//  Created by Tsung-Huan Tsai on 10/6/14.
//  Copyright (c) 2014 Tsung-Huan Tsai. All rights reserved.
//

#ifndef _V3_H
#define _V3_H

#include <string>
#include <math.h>
#include <iostream>

using namespace std;

class V3
{
protected:
    double vector[3]; //the x y and z
    //double crossProduct[3]; //the cross product with size 3
    
public:
    V3(double x, double y, double z); //constructor for x y and z
    V3& operator+(const V3&); //overload operator +
    V3& operator-(const V3&); //overload operator -
	V3& operator=(const V3&); //overload operator =
    void negate();
    void scale(double s); //scaler function
    double dot(const V3&, const V3&); //dot product
    void cross(const V3&, const V3&); //cross product
    double length(); //length of the vector
    void normalize(); //normalize the vector
    void print(string comment); //print x, y and z
	double getX(); // get the x element
	double getY(); // get the y element
	double getZ(); // get the z element
    void setX(double x);
    void setY(double y);
    void setZ(double z);
};

#endif
