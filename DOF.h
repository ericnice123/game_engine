#ifndef DOF_H
#define DOF_H

#include "V3.h"

class DOF
{
public:
    void setValue(float value);
    float getValue();
    void setMinMax(float min, float max);
private:
    float value;
    float min, max;
};
#endif