#ifndef GROUND_H
#define GROUND_H

#include <GLUT/glut.h>
#include "V3.h"

class Ground
{
    V3 pos1 = V3(0,0,0);
    V3 pos2 = V3(0,0,0);
    V3 pos3 = V3(0,0,0);
    V3 pos4 = V3(0,0,0);

public:
    Ground(V3 pos1, V3 pos2, V3 pos3, V3 pos4);
    void Draw();
};

#endif