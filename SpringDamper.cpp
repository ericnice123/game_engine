#include "SpringDamper.h"


SpringDamper::SpringDamper(Particle *p1, Particle *p2)
{
    P1 = p1;
    P2 = p2;
    
    // init all the constants
    SpringConstant = 200000;
    DampingFactor = 100000;
    
    // init the restLength
    V3 p1_pos = V3(p1->Position.getX(), p1->Position.getY(), p1->Position.getZ());
    V3 p2_pos = V3(p2->Position.getX(), p2->Position.getY(), p2->Position.getZ());
    V3 restL = p1_pos - p2_pos;
    RestLength = (float)restL.length();
    //cout << RestLength << endl;
}


void SpringDamper::ComputeForce()
{
    // Fs = -KsX = -Ks(l0 - l)
    // Fd = -KdV = -Kd(V1 - V2)
    // Fsd = -Ks(l0 - l) - Kd(V1 - V2)
    
    // Fs = -KsX = -Ks(l0 - l)
    V3 p1_pos = V3(P1->Position.getX(), P1->Position.getY(), P1->Position.getZ());
    V3 p2_pos = V3(P2->Position.getX(), P2->Position.getY(), P2->Position.getZ());
    V3 d = p2_pos - p1_pos;
    float l = (float)d.length();
    float deltaLength = RestLength - l; // l0 - l
    float fs = -SpringConstant * deltaLength;
    
    
    // Fd = -KdV = -Kd(V1 - V2)
    d.normalize();
    V3 e = d;
    float v1 = e.dot(e, P1->Velocity);
    float v2 = e.dot(e, P2->Velocity);
    float deltaV = v1 - v2;
    float kd = -DampingFactor * deltaV;
    
    // Fsd = -Ks(l0 - l) - Kd(V1 - V2)
    float fsd = fs + kd;
    
    // f1 and f2
    e.scale(fsd);
    V3 f1 = V3(e.getX(), e.getY(), e.getZ());
    V3 f2 = V3(-f1.getX(), -f1.getY(), -f1.getZ());
    
    P1->ApplyForce(f1);
    P2->ApplyForce(f2);
}

void SpringDamper::Draw()
{
    glDisable(GL_LIGHTING);
    glColor3d(1,1,1);
    glBegin(GL_LINES);
    glVertex3f(P1->Position.getX(), P1->Position.getY(), P1->Position.getZ());
    glVertex3f(P2->Position.getX(), P2->Position.getY(), P2->Position.getZ());
    glEnd();
    glEnable(GL_LIGHTING);
}