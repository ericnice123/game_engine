#include "Joint.h"

Joint::Joint(char name[], int &jointNum)
{
    rotxlimitfrom = -50;
    rotxlimitto = 50;
    rotylimitfrom = -50;
    rotylimitto = 50;
    rotzlimitfrom = -50;
    rotzlimitto = 50;
    posex = posey = posez = 0;
    for(int i = 0; i < sizeof(this->name); i++)
    {
        this->name[i] = name[i];
    }
    
    jointnumber = jointNum;
    jointNum++;
    jointnumber_ptr = &jointNum;
}

void Joint::update(Matrix4 parent, vector<Matrix4>& world)
{
    //translate matrix
    Matrix4 translateMatrix;
    translateMatrix.makeTranslate(offsetx, offsety, offsetz);
    
    //checking poses
    if(!(posex >= rotxlimitfrom && posex <= rotxlimitto))
    {
        if(posex <rotxlimitfrom) posex = rotxlimitfrom;
        else posex = rotxlimitto;
    }
    
    if(!(posey >= rotylimitfrom && posey <= rotylimitto))
    {
        if(posey < rotylimitfrom) posey = rotylimitfrom;
        else posey = rotylimitto;
    }
    
    if(!(posez >= rotzlimitfrom && posez <= rotzlimitto))
    {
        if(posez < rotzlimitfrom) posez = rotzlimitfrom;
        else posez = rotzlimitto;
    }
    
    //rotation x matrix
    Matrix4 rotationX;
    double x_rotation = (double)((posex * 180.0) / (M_PI));
    rotationX.makeRotateX(x_rotation);
    
    //rotation y matrix
    Matrix4 rotationY;
    double y_rotation = (double)((posey * 180.0) / (M_PI));
    rotationY.makeRotateY(y_rotation);
    
    //rotation z matrix
    Matrix4 rotationZ;
    double z_rotation = (double)((posez * 180.0) / (M_PI));
    rotationZ.makeRotateZ(z_rotation);

    // compute localMatrix(translate * parent * local(rotations))
    localMatrix = translateMatrix* rotationZ * rotationY * rotationX;
    
    // compute worldMatrix parent * local
    worldMatrix = parent *localMatrix;
    world.push_back(worldMatrix);
    
    //recursively call update on children
    for(int i = 0; i < children.size(); i++)
    {
        children[i]->update(worldMatrix, world);
    }
}

void Joint::draw()
{
    // do some openGL
    Matrix4 copy_world = worldMatrix;
    copy_world.transpose();
    glLoadMatrixd(copy_world.getPointer());
    drawWireBox(minx, miny, minz, maxx, maxy, maxz);
    
    // recursively call draw on children
    for(int i = 0; i < children.size(); i++)
    {
        children[i]->draw();
    }
}

Matrix4& Joint::getWorldMatrix()
{
    return worldMatrix;
}

bool Joint::load(Tokenizer &token, vector<Joint*> &joint)
{
    token.FindToken("{");
    while(1)
    {
        char temp[256];
        token.GetToken(temp);
        if(strcmp(temp,"offset")==0)
        {
            offsetx=token.GetFloat();
            offsety=token.GetFloat();
            offsetz=token.GetFloat();
            //cout << "offsets: " << offsetx << " " <<offsety << " " << offsetz << endl;
        }
        else if(strcmp(temp, "boxmin")==0)
        {
            minx=token.GetFloat();
            miny=token.GetFloat();
            minz=token.GetFloat();
            //cout << "boxmin: " << minx << " " << miny << " " << minz << endl;
        }
        else if(strcmp(temp, "boxmax")==0)
        {
            maxx=token.GetFloat();
            maxy=token.GetFloat();
            maxz=token.GetFloat();
            //cout << "boxmax: " << maxx << " " << maxy << " " << maxz << endl;
        }
        else if(strcmp(temp, "pose")==0)
        {
            posex=token.GetFloat();
            posey=token.GetFloat();
            posez=token.GetFloat();
            dofs[0].setValue(posex);
            dofs[1].setValue(posey);
            dofs[2].setValue(posez);
            //cout << "pose: " << posex << " " << posey << " " << posez << endl;
        }
        else if(strcmp(temp, "rotxlimit")==0)
        {
            rotxlimitfrom=token.GetFloat();
            rotxlimitto=token.GetFloat();
            dofs[0].setMinMax(rotxlimitfrom, rotxlimitto);
            //cout << "rotxlimit: " << rotxlimitfrom << " " << rotxlimitto << endl;
        }
        else if(strcmp(temp, "rotylimit")==0)
        {
            rotylimitfrom=token.GetFloat();
            rotylimitto=token.GetFloat();
            dofs[1].setMinMax(rotylimitfrom, rotylimitto);
            //cout << "rotylimit: " << rotylimitfrom << " " << rotylimitto << endl;
        }
        else if(strcmp(temp, "rotzlimit")==0)
        {
            rotzlimitfrom=token.GetFloat();
            rotzlimitto=token.GetFloat();
            dofs[2].setMinMax(rotylimitfrom, rotylimitto);
            //cout << "rotzlimit: " << rotzlimitfrom << " " << rotzlimitto << endl;
        }
        else if(strcmp(temp,"balljoint")==0)
        {
            char name[256];
            token.GetToken(name);
            
            Joint *jnt=new Joint(name, *jointnumber_ptr);
            joint.push_back(jnt);
            jnt->load(token, joint);
            this->addChild(jnt);
        }
        else if(strcmp(temp,"}")==0) return true;
        else token.SkipLine(); // Unrecognized token
    }
}

void Joint::addChild(Joint* child)
{
    children.push_back(child);
}

char* Joint::getName()
{
    return name;
}

float& Joint::getPoseX()
{
    return posex;
}

float& Joint::getPoseY()
{
    return posey;
}

float& Joint::getPoseZ()
{
    return posez;
}

float Joint::getrotxMin()
{
    return rotxlimitfrom;
}

float Joint::getrotxMax()
{
    return rotxlimitto;
}

float Joint::getrotyMin()
{
    return rotylimitfrom;
}

float Joint::getrotyMax()
{
    return rotylimitto;
}

float Joint::getrotzMin()
{
    return rotzlimitfrom;
}

float Joint::getrotzMax()
{
    return rotzlimitto;
}

void Joint::print()
{
    cout << "name: " << name << endl;
    cout << "jointnumber" << jointnumber << endl;
    cout << "offsets: " << offsetx << " " << offsety << " " << offsetz << endl;
    cout << "boxmin: " << minx << " " << miny << " " << minz << endl;
    cout << "boxmax: " << maxx << " " << maxy << " " << maxz << endl;
    cout << "rotxlimit: " << rotxlimitfrom << " " << rotxlimitto << endl;
    cout << "rotylimit: " << rotylimitfrom << " " << rotylimitto << endl;
    cout << "rotzlimit: " << rotzlimitfrom << " " << rotzlimitto << endl;
    cout << "pose: " << posex << " " << posey << " " << posez << endl;

    for(int i = 0; i < children.size(); i++)
        children[i]->print();
}