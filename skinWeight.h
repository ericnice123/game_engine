#ifndef SKINWEIGHT_H
#define SKINWEIGHT_H

#include "token.h"
#include <vector>
#include <iostream>

using namespace std;

class SkinWeight
{
public:
    int howManyJoints;
    vector<int> joints;
    vector<float> weights;
};

#endif