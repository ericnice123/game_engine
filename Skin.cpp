#include "Skin.h"
#include <GLUT/glut.h>

bool Skin::load(const char *file, Skeleton* skeleton)
{
    positions.clear();
    triangles.clear();
    normals.clear();
    bindings_inverse.clear();
    skinweights.clear();
    Tokenizer token;
    token.Open(file);
    token.FindToken("positions");
    loadPositions(token);
    
    token.FindToken("normals");
    loadNormals(token);
    
    token.FindToken("skinweights");
    loadSkinWeights(token);
    
    token.FindToken("triangles");
    loadTriangles(token);
    
    token.FindToken("bindings");
    loadBindings(token);
    
    this->skeleton = skeleton;
    // Finish
    token.Close();
    return true;
}


void Skin::update()
{
    //compute the v'
    computeV_prime();
    //compute n'
    computeN_prime();
}

void Skin::draw()
{
    glLoadIdentity();
    glBegin(GL_TRIANGLES);
    for(int i = 0; i < triangles.size(); i++)
    {
        glNormal3f(normals_prime[triangles[i].index_x].x,
                   normals_prime[triangles[i].index_x].y,
                   normals_prime[triangles[i].index_x].z);
        glVertex3f(positions_prime[triangles[i].index_x].x,
                   positions_prime[triangles[i].index_x].y,
                   positions_prime[triangles[i].index_x].z);
        
        glNormal3f(normals_prime[triangles[i].index_y].x,
                   normals_prime[triangles[i].index_y].y,
                   normals_prime[triangles[i].index_y].z);
        glVertex3f(positions_prime[triangles[i].index_y].x,
                   positions_prime[triangles[i].index_y].y,
                   positions_prime[triangles[i].index_y].z);
        
        glNormal3f(normals_prime[triangles[i].index_z].x,
                   normals_prime[triangles[i].index_z].y,
                   normals_prime[triangles[i].index_z].z);
        glVertex3f(positions_prime[triangles[i].index_z].x,
                   positions_prime[triangles[i].index_z].y,
                   positions_prime[triangles[i].index_z].z);
    }
    glEnd();
}

void Skin::drawNormals()
{
    glDisable(GL_LIGHTING);
    glLoadIdentity();
    glBegin(GL_LINES);
    for(int i = 0; i < triangles.size(); i++)
    {
        glVertex3f(positions_prime[triangles[i].index_x].x,
                   positions_prime[triangles[i].index_x].y,
                   positions_prime[triangles[i].index_x].z);
        glVertex3f(positions_prime[triangles[i].index_x].x + normals_prime[triangles[i].index_x].x,
                   positions_prime[triangles[i].index_x].y + normals_prime[triangles[i].index_x].y,
                   positions_prime[triangles[i].index_x].z + normals_prime[triangles[i].index_x].z);
        
        glVertex3f(positions_prime[triangles[i].index_y].x,
                   positions_prime[triangles[i].index_y].y,
                   positions_prime[triangles[i].index_y].z);
        glVertex3f(positions_prime[triangles[i].index_y].x + normals_prime[triangles[i].index_y].x,
                   positions_prime[triangles[i].index_y].y + normals_prime[triangles[i].index_y].y,
                   positions_prime[triangles[i].index_y].z + normals_prime[triangles[i].index_y].z);

        glVertex3f(positions_prime[triangles[i].index_z].x,
                   positions_prime[triangles[i].index_z].y,
                   positions_prime[triangles[i].index_z].z);
        glVertex3f(positions_prime[triangles[i].index_z].x + normals_prime[triangles[i].index_z].x,
                   positions_prime[triangles[i].index_z].y + normals_prime[triangles[i].index_z].y,
                   positions_prime[triangles[i].index_z].z + normals_prime[triangles[i].index_z].z);
    }
    glEnd();
    glEnable(GL_LIGHTING);
}

void Skin::computeV_prime()
{
    positions_prime.clear();
    for(int i = 0; i < positions.size(); i++)
    {
        Vector4 copy_v = Vector4(positions[i].x, positions[i].y, positions[i].z, 1);
        Vector4 v_prime = Vector4(0,0,0,0);
        for(int j = 0; j < skinweights[i].howManyJoints; j++)
        {
            float weight = skinweights[i].weights[j];
            Matrix4 world_i = skeleton->getWorldMatrix(skinweights[i].joints[j]);
            world_i.scaleAll(weight);
            Matrix4 binding_inverse_i = bindings_inverse[skinweights[i].joints[j]];
            Vector4 v = world_i * binding_inverse_i * copy_v;
            v_prime = v_prime + v;
        }
        Vertex v_prime_copy;
        v_prime_copy.x = (float)v_prime.vector4[0];
        v_prime_copy.y = (float)v_prime.vector4[1];
        v_prime_copy.z = (float)v_prime.vector4[2];
        //cout << v_prime_copy.x << " " << v_prime_copy.y << " " << v_prime_copy.z << endl;
        positions_prime.push_back(v_prime_copy);
    }
    //cout << positions_prime.size() << endl;
}

void Skin::computeN_prime()
{
    normals_prime.clear();
    for(int i = 0; i < normals.size(); i++)
    {
        Vector4 copy_n = Vector4(normals[i].x, normals[i].y, normals[i].z, 1);
        Vector4 n_prime = Vector4(0,0,0,0);
        for(int j = 0; j < skinweights[i].howManyJoints; j++)
        {
            float weight = skinweights[i].weights[j];
            Matrix4 world_i = skeleton->getWorldMatrix(skinweights[i].joints[j]);
            world_i.scaleAll(weight);
            Matrix4 binding_inverse_i = bindings_inverse[skinweights[i].joints[j]];
            Vector4 n = world_i * binding_inverse_i * copy_n;
            n_prime = n_prime + n;
        }
        V3 normalize = V3((float)n_prime.vector4[0],
                          (float)n_prime.vector4[1],
                          (float)n_prime.vector4[2]);
        normalize.normalize();
        Vertex n_prime_copy;
        n_prime_copy.x = normalize.getX();
        n_prime_copy.y = normalize.getY();
        n_prime_copy.z = normalize.getZ();
        //cout << n_prime_copy.x << " " << n_prime_copy.y << " " << n_prime_copy.z << endl;
        normals_prime.push_back(n_prime_copy);
    }
    //cout << normals_prime.size() << endl;
}

void Skin::loadPositions(Tokenizer &token)
{
    token.SkipLine();
    char c = token.CheckChar();
    while(c != '}')
    {
        Vertex v;
        v.x = token.GetFloat();
        v.y = token.GetFloat();
        v.z = token.GetFloat();
        positions.push_back(v);
        token.SkipLine();
        c = token.CheckChar();
    }
}

void Skin::loadNormals(Tokenizer &token)
{
    token.SkipLine();
    char c = token.CheckChar();
    while(c != '}')
    {
        Vertex v;
        v.x = token.GetFloat();
        v.y = token.GetFloat();
        v.z = token.GetFloat();
        normals.push_back(v);
        token.SkipLine();
        c = token.CheckChar();
    }
    /*for(int i = 0; i < normals.size(); i++)
    {
        cout << normals[i].x << " " << normals[i].y << " " << normals[i].z << endl;
    }
    cout << normals.size() << endl;*/
}

void Skin::loadTriangles(Tokenizer &token)
{
    token.SkipLine();
    char c = token.CheckChar();
    while(c != '}')
    {
        Triangle t;
        t.index_x = token.GetInt();
        t.index_y = token.GetInt();
        t.index_z = token.GetInt();
        triangles.push_back(t);
        token.SkipLine();
        c = token.CheckChar();
    }
    /*for(int i = 0; i < triangles.size(); i++)
    {
        cout << triangles[i].index_x << " " << triangles[i].index_y << " " << triangles[i].index_z << endl;
    }
    cout << triangles.size() << endl;*/
}

void Skin::loadBindings(Tokenizer &token)
{
    token.SkipLine();
    token.SkipWhitespace();
    char c = token.CheckChar();
    while(c == 'm')
    {
        token.SkipLine();
        Matrix4 m;
        float a_x, a_y, a_z,
        b_x, b_y, b_z,
        c_x, c_y, c_z,
        d_x, d_y, d_z;
        
        a_x = token.GetFloat();
        a_y = token.GetFloat();
        a_z = token.GetFloat();
        V3 x = V3(a_x, a_y, a_z);
        token.SkipLine();
        
        b_x = token.GetFloat();
        b_y = token.GetFloat();
        b_z = token.GetFloat();
        V3 y = V3(b_x, b_y, b_z);
        token.SkipLine();
        
        c_x = token.GetFloat();
        c_y = token.GetFloat();
        c_z = token.GetFloat();
        V3 z = V3(c_x, c_y, c_z);
        token.SkipLine();
        
        d_x = token.GetFloat();
        d_y = token.GetFloat();
        d_z = token.GetFloat();
        Vector4 t = Vector4(d_x, d_y, d_z, 1);
        //t.print("d: ");
        token.SkipLine();
        token.SkipLine();
        
        m.identity();
        m.setX(x);
        m.setY(y);
        m.setZ(z);
        m.setW_W(t);
        m.inverse();
        //m.print("inverse:");
        
        
        bindings_inverse.push_back(m);
        
        token.SkipWhitespace();
        c = token.CheckChar();
    }
    /*for(int i = 0; i < bindings_inverse.size(); i++)
    {
        bindings_inverse[i].print("b_inverse:");
    }
    cout << bindings_inverse.size() << endl;*/
}

void Skin::loadSkinWeights(Tokenizer &token)
{
    token.SkipLine();
    char c = token.CheckChar();
    while(c != '}')
    {
        SkinWeight s;
        s.howManyJoints = token.GetInt();
        for(int i = 0; i < s.howManyJoints; i++)
        {
            int jointNumber = token.GetInt();
            s.joints.push_back(jointNumber);
            float weight = token.GetFloat();
            s.weights.push_back(weight);
        }
        skinweights.push_back(s);
        token.SkipLine();
        c = token.CheckChar();
    }
    /*for(int i = 0; i < skinweights.size(); i++)
    {
        cout << skinweights[i].howManyJoints << " ";
        for(int j = 0; j < skinweights[i].howManyJoints; j++)
        {
            cout << skinweights[i].joints[j] << " ";
            cout << skinweights[i].weights[j] << " ";
        }
        cout << " " << endl;
    }
    cout << skinweights.size() << endl;*/
}





