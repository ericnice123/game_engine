#ifndef JOINT_H
#define JOINT_H

#include "token.h"
#include "Matrix4.h"
#include "DOF.h"
#include "core.h"
#include <vector>

class Joint
{
public:
    Joint(char [], int &jointNum);
    //~Joint();
    void update(Matrix4 parent, vector<Matrix4> & world);
    void draw();
    bool load(Tokenizer &t, vector<Joint*> &joint);
    void addChild(Joint*);
    Matrix4& getWorldMatrix();
    void print();
    char* getName();
    float& getPoseX();
    float& getPoseY();
    float& getPoseZ();
    float getrotxMin();
    float getrotxMax();
    float getrotyMin();
    float getrotyMax();
    float getrotzMin();
    float getrotzMax();
    float offsetx, offsety, offsetz;
    float posex, posey, posez;
private:
    Joint* parent; // pointer to parent
    vector<Joint*> children; // pointers to children
    DOF dofs[3]; // x y and z
    Matrix4 localMatrix;
    Matrix4 worldMatrix;
    Matrix4 temp;
    float minx, miny, minz, maxx, maxy, maxz;
    float rotxlimitfrom, rotxlimitto, rotylimitfrom, rotylimitto, rotzlimitfrom, rotzlimitto;
    char name[256];
    int jointnumber;
    int* jointnumber_ptr;
};

#endif