#ifndef SKELETON_H
#define SKELETON_H

#include "Joint.h"
#include "token.h"
#include "Matrix4.h"

class Skeleton
{
public:
    bool load(const char *file);
    void update(Matrix4 world);//update the root
    void draw();
    void print();
    Matrix4 getWorldMatrix(int i);
    int numofjoints;
    vector<Joint*> joint;
private:
    Joint* root;
    vector<Matrix4> worldMatrix;
};

#endif