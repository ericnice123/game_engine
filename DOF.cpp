#include "DOF.h"

void DOF::setValue(float value)
{
    this->value = value;
}

float DOF::getValue()
{
    return this->value;
}

void DOF::setMinMax(float min, float max)
{
    this->min = min;
    this->max = max;
}
