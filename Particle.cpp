#include "Particle.h"

void Particle::Update(float deltaTime)
{
    // Compute acceleration (second law)
    Force.scale(1.0/Mass);
    V3 Accel = V3(Force.getX(), Force.getY(), Force.getZ());
    //Accel.print("a");
    // compute new position & velocity
    
    // velocity += accel*deltaTime
    Accel.scale(deltaTime);
    v_x += Accel.getX();
    v_y += Accel.getY();
    v_z += Accel.getZ();
    Velocity.setX(v_x);
    Velocity.setY(v_y);
    Velocity.setZ(v_z);
    //Velocity.print("v");
    
    // position += velocity * deltaTime
    Velocity.scale(deltaTime);
    Position.setX(Position.getX() + Velocity.getX());
    Position.setY(Position.getY() + Velocity.getY());
    Position.setZ(Position.getZ() + Velocity.getZ());
    
    // collision dectection between plane
    if(Position.getY() < -0.1)
    {
        Position.setY(-0.1);
        Velocity.setX((1.0 - friction) * Velocity.getX());
        Velocity.setY(-elasticity * Velocity.getY());
        Velocity.setZ((1.0 - friction) * Velocity.getZ());
    }
    
    //Position.print("position");
    
    // Zero out Force vector
    Force.setX(0);
    Force.setY(0);
    Force.setZ(0);
}


void Particle::Draw()
{
    //cout << Position.getX() << " " << Position.getY() << " " << Position.getZ() << endl;
    glPointSize(2.0);
    glColor3f(1, 0, 0);
    glBegin(GL_POINTS);
    glNormal3f(0,0,1);
    glVertex3f(Position.getX(), Position.getY(), Position.getZ());
    glEnd();
}

void Particle::Build(V3 initPos, float mass)
{
    // set the mass
    Mass = mass;
    
    // set the Position
    this->Position.setX(initPos.getX());
    this->Position.setY(initPos.getY());
    this->Position.setZ(initPos.getZ());
}

void Particle::SetPosition(V3 &pos)
{
    this->Position.setX(pos.getX());
    this->Position.setY(pos.getY());
    this->Position.setZ(pos.getZ());
}