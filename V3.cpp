#include "V3.h"

V3::V3(double x, double y, double z)
{
    vector[0] = x;
    vector[1] = y;
    vector[2] = z;
}

V3& V3::operator+(const V3& v2)
{
    for(int i = 0; i < 3; i++)
    {
        vector[i] = v2.vector[i] + vector[i];
    }
    return *this;
}

V3& V3::operator-(const V3& v2)
{
    for(int i = 0; i < 3; i++)
    {
        vector[i] = vector[i] - v2.vector[i];
    }
    return *this;
}

V3& V3::operator=(const V3& v2)
{
	for (int i = 0; i < 3; i++)
	{
		vector[i] = v2.vector[i];
	}
	return *this;
}

void V3::negate()
{
    for(int i = 0; i < 3; i++)
    {
        vector[i] = -vector[i];
    }
}

void V3::scale(double s)
{
    for(int i = 0; i < 3; i++)
    {
        vector[i] = s * vector[i];
    }
}

double V3::dot(const V3& v1, const V3& v2)
{
    double result = 0;
    for(int i = 0; i < 3; i++)
        result = result + v1.vector[i] * v2.vector[i];
    return result;
}

void V3::cross(const V3& v1, const V3& v2)
{
    double x = v1.vector[1] * v2.vector[2] - v1.vector[2] * v2.vector[1];
    double y = v1.vector[2] * v2.vector[0] - v1.vector[0] * v2.vector[2];
    double z = v1.vector[0] * v2.vector[1] - v1.vector[1] * v2.vector[0];
    
    vector[0] = x;
    vector[1] = y;
    vector[2] = z;
}

double V3::length()
{
    double x = vector[0] * vector[0];
    double y = vector[1] * vector[1];
    double z = vector[2] * vector[2];
    double result = sqrt(x+y+z);
    return result;
}

void V3::normalize()
{
    double length = this->length();
    if(length != 0)
    {
        for(int i = 0; i < 3; i++)
        vector[i] = vector[i] / length;
    }
}

double V3::getX()
{
	return vector[0];
}

double V3::getY()
{
	return vector[1];
}

double V3::getZ()
{
	return vector[2];
}

void V3::setX(double x)
{
    vector[0] = x;
}

void V3::setY(double y)
{
    vector[1] = y;
}

void V3::setZ(double z)
{
    vector[2] = z;
}

void V3::print(string comment)
{
    cout << comment << "\t" << vector[0] << "\t" << vector[1] << "\t" << vector[2] << endl;
}












