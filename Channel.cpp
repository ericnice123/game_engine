#include "Channel.h"

bool Channel::Load(Tokenizer &token)
{
    token.FindToken("extrapolate");
    token.GetToken(extra_in);
    token.GetToken(extra_out);
    //cout << "extrapolate " << extra_in << " " << extra_out << endl;
    
    token.FindToken("keys");
    numOfKeys = token.GetInt();
    //cout << "keys " << numOfKeys << endl;
    
    token.SkipLine();
    token.SkipWhitespace();
    for(int i = 0; i < numOfKeys; i++)
    {
        Keyframe keyframe;
        keyframe.Time = token.GetFloat();
        keyframe.Value = token.GetFloat();
        
        token.SkipWhitespace();
        char check = token.CheckChar();
        if(!(check == 's' || check == 'f' || check == 'l'))
        {
            keyframe.TangentIn = token.GetFloat();
            keyframe.RuleIn = 'i';
        }
        else
        {
            keyframe.RuleIn = token.GetChar();
        }
        
        token.FindToken(" ");
        char c = token.CheckChar();
        if(!(c == 's' || c == 'f' || c == 'l'))
        {
            keyframe.TangentOut = token.GetFloat();
            keyframe.RuleOut = 'o';
        }
        else
        {
            keyframe.RuleOut = token.GetChar();
        }
        keyframes.push_back(keyframe);
        token.SkipLine();
        token.SkipWhitespace();
        //cout << keyframe.Time << " " << keyframe.Value << " " <<
        //        keyframe.RuleIn << " " << keyframe.RuleOut << endl;
    }
    return true;
}


void Channel::preComputeTangent()
{
    // assume tanIn is the same as tanOut
    for(int i = 0; i < numOfKeys; i++)
    {
        // tangent in
        if(keyframes[i].RuleIn == 'f')
        {
            keyframes[i].TangentIn = 0;
            keyframes[i].TangentOut = 0;
        }
        else if(keyframes[i].RuleIn == 's')
        {
            // if it's the first or the last keyframe
            // if we see s on first and last keyframe, use linear
            if((i == numOfKeys -1 || i == 0) && numOfKeys > 1)
            {
                if(i != 0) // at the last keyframe
                {
                    float t0 = keyframes[i-1].Time;
                    float t1 = keyframes[i].Time;
                    float p0 = keyframes[i-1].Value;
                    float p1 = keyframes[i].Value;
                    keyframes[i].TangentIn = (float)((p1 - p0) / (t1 - t0));
                    // also set the tanOut the be the same
                    keyframes[i].TangentOut = keyframes[i].TangentIn;
                    //cout << keyframes[i].TangentIn << " " << keyframes[i].TangentOut << endl;
                }
                else if(i == 0) // at the first keyframe
                {
                    float t0 = keyframes[i].Time;
                    float t1 = keyframes[i+1].Time;
                    float p0 = keyframes[i].Value;
                    float p1 = keyframes[i+1].Value;
                    keyframes[i].TangentOut = (float)((p1 - p0) / (t1 - t0));
                    keyframes[i].TangentIn = keyframes[i].TangentOut;
                    //cout << keyframes[i].TangentIn << " " << keyframes[i].TangentOut << endl;
                }
            }
            else if(numOfKeys > 2) // in middle
            {
                float t0 = keyframes[i-1].Time;
                float t2 = keyframes[i+1].Time;
                float p0 = keyframes[i-1].Value;
                float p2 = keyframes[i+1].Value;
                keyframes[i].TangentIn = (float)((p2 - p0) / (t2 - t0));
                keyframes[i].TangentOut = keyframes[i].TangentIn;
            }
        }
        else if(keyframes[i].RuleIn == 'l')
        {
            if(i == 0 && numOfKeys > 1)
            {
                float t0 = keyframes[i].Time;
                float t1 = keyframes[i+1].Time;
                float p0 = keyframes[i].Value;
                float p1 = keyframes[i+1].Value;
                keyframes[i].TangentOut = (float)((p1 - p0) / (t1 - t0));
                keyframes[i].TangentIn = keyframes[i].TangentOut;
            }
            else if(numOfKeys > 1)// in middle + last
            {
                float t0 = keyframes[i-1].Time;
                float t1 = keyframes[i].Time;
                float p0 = keyframes[i-1].Value;
                float p1 = keyframes[i].Value;
                keyframes[i].TangentIn = (float)((p1 - p0) / (t1 - t0));
                keyframes[i].TangentOut = keyframes[i].TangentIn;
            }
        }
        
        
        /*// tangent out
        if(keyframes[i].RuleOut == 'f')
        {
            keyframes[i].TangentOut = 0;
        }
        else if(keyframes[i].RuleOut == 's')
        {
            // if it's the first or the last keyframe
            if(i == 0 || i == numOfKeys -1)
            {
                if(i == 0)
                {
                    float t0 = keyframes[i].Time;
                    float t1 = keyframes[i+1].Time;
                    float p0 = keyframes[i].Value;
                    float p1 = keyframes[i+1].Value;
                    keyframes[i].TangentOut = (float)((p1 - p0) / (t1 - t0));
                    // also set the tanin to be the same
                    keyframes[i].TangentIn = keyframes[i].TangentOut;
                }
            }
            else // in middle
            {
                float t0 = keyframes[i-1].Time;
                float t2 = keyframes[i+1].Time;
                float p0 = keyframes[i-1].Value;
                float p2 = keyframes[i+1].Value;
                keyframes[i].TangentOut = (float)((p2 - p0) / (t2 - t0));
            }
        }
        else if(keyframes[i].RuleOut == 'l')
        {
            // first and last keyframe
            if(i == numOfKeys -1 && numOfKeys > 1)
            {
                // assume out == in
                keyframes[i].TangentOut = keyframes[i].TangentIn;
            }
            else // in middle + first
            {
                float t0 = keyframes[i].Time;
                float t1 = keyframes[i+1].Time;
                float p0 = keyframes[i].Value;
                float p1 = keyframes[i+1].Value;
                keyframes[i].TangentOut = (float)((p1 - p0) / (t1 - t0));
            }
        }*/
        //cout << keyframes[i].TangentIn << " " << keyframes[i].TangentOut << endl;
    }
}


void Channel::preComputeCoeff()
{
    Matrix4 inverseHermite;
    Vector4 x = Vector4(2, -3, 0, 1);
    Vector4 y = Vector4(-2, 3, 0, 0);
    Vector4 z = Vector4(1, -2, 1, 0);
    Vector4 w = Vector4(1, -1, 0, 0);
    inverseHermite.setX_W(x);
    inverseHermite.setY_W(y);
    inverseHermite.setZ_W(z);
    inverseHermite.setW_W(w);
        
    for(int i = 0; i < numOfKeys; i++)
    {
        if(i != numOfKeys -1)
        {
            float p0 = keyframes[i].Value;
            float p1 = keyframes[i+1].Value;
            float v0 = keyframes[i].TangentOut;
            float v1 = keyframes[i+1].TangentIn;
            float t1_t0 = keyframes[i+1].Time - keyframes[i].Time;
            Vector4 p0_v1 = Vector4(p0, p1, t1_t0 * v0, t1_t0 * v1);
            Vector4 a_b_c_d = inverseHermite * p0_v1;
            keyframes[i].a = a_b_c_d.vector4[0];
            keyframes[i].b = a_b_c_d.vector4[1];
            keyframes[i].c = a_b_c_d.vector4[2];
            keyframes[i].d = a_b_c_d.vector4[3];
            //cout << keyframes[i].a << " " << keyframes[i].b << " " <<
            //          keyframes[i].c << " " << keyframes[i].d << endl;
        }
    }
}

float Channel::Evaluate(float time)
{
    // time less than first keyframe
    if(time < keyframes[0].Time) // less than first keyframe
    {
        if(strcmp(extra_in, "constant") == 0)
        {
            return keyframes[0].Value; // return the pose value
        }
        else if(strcmp(extra_in, "cycle") == 0)
        {
            if(numOfKeys == 1)
            {
                return keyframes[0].Value;
            }
            else
            {
                float cycle_time = fmod(keyframes[0].Time - time,(keyframes[keyframes.size()-1].Time - keyframes[0].Time));
                
                if(cycle_time == 0) return keyframes[0].Value;
                
                cycle_time = keyframes[0].Time + cycle_time;
                
                for(int i = 0; i < numOfKeys; i++)
                {
                    // if is one of the keyframe
                    if(cycle_time == keyframes[i].Time)
                    {
                        return keyframes[i].Value;
                    }
                    // if is in between
                    else if(i != numOfKeys - 1)
                    {
                        if(cycle_time > keyframes[i].Time && cycle_time < keyframes[i+1].Time)
                        {
                            // compute pose
                            float t0 = keyframes[i].Time;
                            float t1 = keyframes[i+1].Time;
                            float u = (float)((cycle_time - t0) / (t1 - t0));
                            return keyframes[i].a * (u * u * u) + keyframes[i].b * (u * u) + keyframes[i].c * u + keyframes[i].d;
                        }
                    }
                }
            }
        }
        else if(strcmp(extra_in, "cycle_offset") == 0)
        {
            if(numOfKeys == 1) return keyframes[0].Value;
            else
            {
                //float cycle_time = fmod(time,(keyframes[keyframes.size()-1].Time - keyframes[0].Time));
                return 0;
            }
        }
        else if(strcmp(extra_in, "linear") == 0)
        {
            return 0;
        }
        else if(strcmp(extra_in, "bounce") == 0)
        {
            return 0;
        }
    }
    // time greater than last keyframe
    else if(time > keyframes[keyframes.size()-1].Time)
    {
        if(strcmp(extra_out, "constant") == 0)
        {
            return keyframes[keyframes.size()-1].Value; // return the pose value
        }
        else if(strcmp(extra_out, "cycle") == 0)
        {
            if(numOfKeys == 1)
            {
                return keyframes[0].Value;
            }
            else
            {
                float cycle_time = fmod(time - keyframes[keyframes.size()-1].Time,(keyframes[keyframes.size()-1].Time - keyframes[0].Time));
                
                if(cycle_time == 0) return keyframes[0].Value;
                
                cycle_time = keyframes[0].Time + cycle_time;
                
                for(int i = 0; i < numOfKeys; i++)
                {
                    // if is one of the keyframe
                    if(cycle_time == keyframes[i].Time)
                    {
                        return keyframes[i].Value;
                    }
                    // if is in between
                    else if(i != numOfKeys - 1)
                    {
                        if(cycle_time > keyframes[i].Time && cycle_time < keyframes[i+1].Time)
                        {
                            // compute pose
                            float t0 = keyframes[i].Time;
                            float t1 = keyframes[i+1].Time;
                            float u = (float)((cycle_time - t0) / (t1 - t0));
                            return keyframes[i].a * u * u * u + keyframes[i].b * u * u + keyframes[i].c * u + keyframes[i].d;
                        }
                    }
                }
            }
        }
        else if(strcmp(extra_out, "cycle_offset") == 0)
        {
            if(numOfKeys == 1) return keyframes[0].Value;
            else
            {
                // do linear interpolation to find how many cycles
                int how_many_cycles = (int)((time - keyframes[0].Time) /
                                            (keyframes[keyframes.size()-1].Time - keyframes[0].Time));
                
                float cycle_time = fmod(time,(keyframes[keyframes.size()-1].Time - keyframes[0].Time));
                
                if(cycle_time == 0)
                {
                    //cout << "yolo" << endl;
                    return (float)(keyframes[0].Value + (float)(how_many_cycles * keyframes[0].Value));
                }
                
                cycle_time = keyframes[0].Time + cycle_time;

                for(int i = 0; i < numOfKeys; i++)
                {
                    // if is one of the keyframe
                    if(cycle_time == keyframes[i].Time)
                    {
                        return keyframes[i].Value + how_many_cycles * (how_many_cycles * (keyframes[i+1].Value - keyframes[i].Value));
                    }
                    // if is in between
                    else if(i != numOfKeys - 1)
                    {
                        if(cycle_time > keyframes[i].Time && cycle_time < keyframes[i+1].Time)
                        {
                            // compute pose
                            float t0 = keyframes[i].Time;
                            float t1 = keyframes[i+1].Time;
                            float u = (float)((cycle_time - t0) / (t1 - t0));
                            float x = (keyframes[i].a * u * u * u + keyframes[i].b * u * u + keyframes[i].c * u + keyframes[i].d);
                            //cout << x << endl;
                            float result = (x + (how_many_cycles * (keyframes[i+1].Value - keyframes[i].Value)));
                            return result;
                        }
                    }
                }
            }
        }
        else if(strcmp(extra_out, "linear") == 0)
        {
            return 0;
        }
        else if(strcmp(extra_out, "bounce") == 0)
        {
            return 0;
        }
    }
    // if time equals any of the keyframes
    for(int i = 0; i < numOfKeys; i++)
    {
        if(time == keyframes[i].Time)
            return keyframes[i].Value;
    }
    // if time is in between of any two keyframes
    for(int i = 0; i < numOfKeys; i++)
    {
        if(i != numOfKeys - 1)
        {
            if(time > keyframes[i].Time && time < keyframes[i+1].Time)
            {
                // compute pose
                float t0 = keyframes[i].Time;
                float t1 = keyframes[i+1].Time;
                float u = (float)((time - t0) / (t1 - t0));
                return keyframes[i].a * u * u * u + keyframes[i].b * u * u + keyframes[i].c * u + keyframes[i].d;
            }
        }
    }
    return -1; // if theres error
}

