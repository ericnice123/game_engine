#include "glui.h"


Glui::Glui(Skeleton* skel, int &window)
{
    value = 0;
    skeleton = skel;
    GLUI *glui_x = GLUI_Master.create_glui("GLUI_X", 0, 700, 0);
    GLUI *glui_y = GLUI_Master.create_glui("GLUI_Y", 0, 900, 0);
    GLUI *glui_z = GLUI_Master.create_glui("GLUI_Z", 0, 1100, 0);
    for(int i = 0; i < skel->joint.size(); i++)
    {
        GLUI_Spinner *joint_x_spinner =
        glui_x->add_spinner(skel->joint[i]->getName(), GLUI_SPINNER_FLOAT, &skel->joint[i]->getPoseX());
        joint_x_spinner->set_float_limits(skel->joint[i]->getrotxMin(), skel->joint[i]->getrotxMax());
        
        GLUI_Spinner *joint_y_spinner =
        glui_y->add_spinner(skel->joint[i]->getName(), GLUI_SPINNER_FLOAT, &skel->joint[i]->getPoseY());
        joint_y_spinner->set_float_limits(skel->joint[i]->getrotyMin(), skel->joint[i]->getrotyMax());
        
        GLUI_Spinner *joint_z_spinner =
        glui_z->add_spinner(skel->joint[i]->getName(), GLUI_SPINNER_FLOAT, &skel->joint[i]->getPoseZ());
        joint_z_spinner->set_float_limits(skel->joint[i]->getrotzMin(), skel->joint[i]->getrotzMax());
    }
    
    
    glui_x->set_main_gfx_window(window);
    glui_y->set_main_gfx_window(window);
    glui_z->set_main_gfx_window(window);
    GLUI_Master.set_glutIdleFunc(myGlutIdle);
}

void Glui::myGlutIdle(void)
{
    glutPostRedisplay();
}