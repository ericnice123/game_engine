////////////////////////////////////////
// tester.cpp
////////////////////////////////////////

#include "tester.h"
#include <GLUI/GLUI.h>


#define WINDOWTITLE	"Spinning Cube"

////////////////////////////////////////////////////////////////////////////////

static Tester *TESTER;

int main(int argc, char **argv) {
	glutInit(&argc, argv);
	TESTER = new Tester(argc,argv);
	glutMainLoop();
	return 0;
}

////////////////////////////////////////////////////////////////////////////////

// These are really HACKS to make glut call member functions instead of static functions
static void display()									{TESTER->Draw();}
static void idle()										{TESTER->Update();}
static void resize(int x,int y)							{TESTER->Resize(x,y);}
static void keyboard(unsigned char key,int x,int y)		{TESTER->Keyboard(key,x,y);}
static void mousebutton(int btn,int state,int x,int y)	{TESTER->MouseButton(btn,state,x,y);}
static void mousemotion(int x, int y)					{TESTER->MouseMotion(x,y);}

////////////////////////////////////////////////////////////////////////////////

Tester::Tester(int argc,char **argv) {
    if(argc == 1)
    {
        skeleton.load("/Users/Tsung-Huan/Documents/CSE169/project1_base/wasp/wasp.skel");
        skin.load("/Users/Tsung-Huan/Documents/CSE169/project1_base/wasp/wasp.skin", &skeleton);
        animClip.load("/Users/Tsung-Huan/Documents/CSE169/project1_base/wasp/wasp_walk.anim", &skeleton);
    }
    else if(argc == 2)
    {
        if(!skeleton.load(argv[1])) exit(0);
    }
    else if(argc > 2)
    {
        skeleton.load(argv[1]);
        skin.load(argv[2], &skeleton);
    }
    time = 0;
    
    // init the particleSystem
    particleSystem.Build(1600, 40, 40);
    
	WinX=640;
	WinY=480;
	LeftDown=MiddleDown=RightDown=false;
	MouseX=MouseY=0;

	// Create the window
	glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH );
	glutInitWindowSize( WinX, WinY );
	glutInitWindowPosition( 0, 0 );
	WindowHandle = glutCreateWindow( WINDOWTITLE );
	glutSetWindowTitle( WINDOWTITLE );
	glutSetWindow( WindowHandle );

	// Background color
    //glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);            	      // enable depth buffering
    glClear(GL_DEPTH_BUFFER_BIT);       	      // clear depth buffer
    glClearColor(0.0, 0.0, 0.0, 0.0);   	      // set clear color to black
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);  // set polygon drawing mode to fill front and back of each polygon
    //glDisable(GL_CULL_FACE);     // disable backface culling to render both sides of polygons
    glMatrixMode(GL_PROJECTION);
    
    // Enable Lighting
    glEnable(GL_LIGHT0);
    //glEnable(GL_LIGHT1);
    glEnable(GL_NORMALIZE);
    glEnable(GL_LIGHTING);
    glShadeModel(GL_SMOOTH);             	      // set shading to smooth
    //glLightModelf(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
    
    //material properties
    GLfloat mat_d[] = {1.0, 0.0, 0.0, 1.0}; // material diffuse
    GLfloat mat_s[] = {0.0, 0.0, 0.0, 1.0}; // material specular
    GLfloat brightness[] = {50.0};
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, mat_d);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_s);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, brightness);
    
    //define lightsource
    GLfloat light_ambient[] = { 1.0, 0.0, 0.0, 1.0 };
    GLfloat light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat light_specular[] = { 0.0, 0.0, 0.0, 1.0 };
    GLfloat light_position[] = { 0.0, 5.0, 10.0, 0.0 };
    
    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    
    /*GLfloat light_diffuse[] = {1.0, 1.0, 1.0, 1.0};
    GLfloat light_position[] = {1.0, 1.0, 1.0, 0.0};
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glLightfv(GL_LIGHT0, GL_AMBIENT, light_diffuse);*/
    
    /*// second light
    GLfloat light_diffuse2[] = {1.0, 1.0, 1.0, 1.0};
    GLfloat light_position2[] = {-10.0, 0.0, 10.0, 0.0};
    glLightfv(GL_LIGHT1, GL_POSITION, light_position2);
    glLightfv(GL_LIGHT1, GL_AMBIENT, light_diffuse2);*/
    
    
    /*float specular[]  = {1.0, 1.0, 1.0, 1.0};
    float shininess[] = {100.0};
    float position[]  = {0.0, 5.0, 0.0, 0.0};	// lightsource position
    // Generate material properties:
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);*/
    //glEnable(GL_COLOR_MATERIAL);
    
    // Generate light source:
    //glLightfv(GL_LIGHT0, GL_POSITION, position);
    
	// Callbacks
	glutDisplayFunc( display );
	glutIdleFunc( idle );
	glutKeyboardFunc( keyboard );
	glutMouseFunc( mousebutton );
	glutMotionFunc( mousemotion );
	glutPassiveMotionFunc( mousemotion );
    glutReshapeFunc( resize );

	// Initialize components
    //glui = new Glui(&skeleton, WindowHandle);
    V3 pos1 = V3(-10, -0.1, 10);
    V3 pos2 = V3(10, -0.1, 10);
    V3 pos3 = V3(10, -0.1, -10);
    V3 pos4 = V3(-10, -0.1, -10);
    ground = new Ground(pos1, pos2, pos3, pos4);
    
	Cam.SetAspect(float(WinX)/float(WinY));
}

////////////////////////////////////////////////////////////////////////////////

Tester::~Tester() {
	glFinish();
	glutDestroyWindow(WindowHandle);
}

////////////////////////////////////////////////////////////////////////////////

void Tester::Update() {
	// Update the components in the world
	Cam.Update();
	Cube.Update();

	// Tell glut to re-display the scene
	glutSetWindow(WindowHandle);
	glutPostRedisplay();
}

////////////////////////////////////////////////////////////////////////////////

void Tester::Reset() {
	Cam.Reset();
	Cam.SetAspect(float(WinX)/float(WinY));

	Cube.Reset();
}

////////////////////////////////////////////////////////////////////////////////

void Tester::Draw() {

	// Begin drawing scene
	glViewport(0, 0, WinX, WinY);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Draw components
	Cam.Draw();		// Sets up projection & viewing matrices
    //glDisable(GL_LIGHTING);
    ground->Draw();
    glEnable(GL_LIGHTING);
    
    if(!particleLine) particleSystem.Draw();
    else particleSystem.DrawDebug();
    for(int i = 0; i < 10; i++)
    particleSystem.Update(0.001);
    /*Matrix4 identity;
    identity.identity();
    skeleton.update(identity);
    if(drawSkeleton)skeleton.draw();
    
    skin.update();
    skin.draw();
    if(drawNormals) skin.drawNormals();
    
    animClip.Evaluate(time);
    time += 0.01;*/
    
	// Finish drawing scene
	glFinish();
	glutSwapBuffers();
}

////////////////////////////////////////////////////////////////////////////////

void Tester::Quit() {
	glFinish();
	glutDestroyWindow(WindowHandle);
	exit(0);
}

////////////////////////////////////////////////////////////////////////////////

void Tester::Resize(int x,int y) {
	WinX = x;
	WinY = y;
	Cam.SetAspect(float(WinX)/float(WinY));
}

////////////////////////////////////////////////////////////////////////////////

void Tester::Keyboard(int key,int x,int y) {
	switch(key) {
		case 0x1b:		// Escape
			Quit();
			break;
		case 'r':
			Reset();
			break;
        case 'n':
            drawNormals = !drawNormals;
            break;
        case 'k':
            drawSkeleton = !drawSkeleton;
            break;
        case 'l':
            particleLine = !particleLine;
            break;
        case 'w':
            for(int i = 0; i < particleSystem.staticP.size(); i++)
            {
                V3 pos = V3(particleSystem.staticP[i]->Position.getX(),
                            particleSystem.staticP[i]->Position.getY() + 0.1,
                            particleSystem.staticP[i]->Position.getZ());
                particleSystem.staticP[i]->SetPosition(pos);
            }
            break;
        case 's':
            for(int i = 0; i < particleSystem.staticP.size(); i++)
            {
                V3 pos = V3(particleSystem.staticP[i]->Position.getX(),
                            particleSystem.staticP[i]->Position.getY() - 0.1,
                            particleSystem.staticP[i]->Position.getZ());
                particleSystem.staticP[i]->SetPosition(pos);
            }
            break;
        case 'a':
            for(int i = 0; i < particleSystem.staticP.size(); i++)
            {
                V3 pos = V3(particleSystem.staticP[i]->Position.getX() - 0.1,
                            particleSystem.staticP[i]->Position.getY(),
                            particleSystem.staticP[i]->Position.getZ());
                particleSystem.staticP[i]->SetPosition(pos);
            }
            break;
        case 'd':
            for(int i = 0; i < particleSystem.staticP.size(); i++)
            {
                V3 pos = V3(particleSystem.staticP[i]->Position.getX() + 0.1,
                            particleSystem.staticP[i]->Position.getY(),
                            particleSystem.staticP[i]->Position.getZ());
                particleSystem.staticP[i]->SetPosition(pos);
            }
            break;
        case 'q':
            for(int i = 0; i < particleSystem.staticP.size(); i++)
            {
                V3 pos = V3(particleSystem.staticP[i]->Position.getX(),
                            particleSystem.staticP[i]->Position.getY(),
                            particleSystem.staticP[i]->Position.getZ() - 0.1);
                particleSystem.staticP[i]->SetPosition(pos);
            }
            break;
        case 'e':
            for(int i = 0; i < particleSystem.staticP.size(); i++)
            {
                V3 pos = V3(particleSystem.staticP[i]->Position.getX(),
                            particleSystem.staticP[i]->Position.getY(),
                            particleSystem.staticP[i]->Position.getZ() + 0.1);
                particleSystem.staticP[i]->SetPosition(pos);
            }
            break;
        case 'z':
            for(int i = 0; i < particleSystem.AD.size(); i++)
            {
                V3 vair = particleSystem.AD[i]->getVair();
                vair.setZ(vair.getZ() + 1.0);
                particleSystem.AD[i]->setVair(vair);
            }
            break;
        case 'x':
            for(int i = 0; i < particleSystem.AD.size(); i++)
            {
                V3 vair = particleSystem.AD[i]->getVair();
                vair.negate();
                particleSystem.AD[i]->setVair(vair);
            }
            break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void Tester::MouseButton(int btn,int state,int x,int y) {
	if(btn==GLUT_LEFT_BUTTON) {
		LeftDown = (state==GLUT_DOWN);
	}
	else if(btn==GLUT_MIDDLE_BUTTON) {
		MiddleDown = (state==GLUT_DOWN);
	}
	else if(btn==GLUT_RIGHT_BUTTON) {
		RightDown = (state==GLUT_DOWN);
	}
}

////////////////////////////////////////////////////////////////////////////////

void Tester::MouseMotion(int nx,int ny) {
	int dx = nx - MouseX;
	int dy = -(ny - MouseY);

	MouseX = nx;
	MouseY = ny;

	// Move camera
	// NOTE: this should really be part of Camera::Update()
	if(LeftDown) {
		const float rate=1.0f;
		Cam.SetAzimuth(Cam.GetAzimuth()+dx*rate);
		Cam.SetIncline(Cam.GetIncline()-dy*rate);
	}
	if(RightDown) {
		const float rate=0.01f;
		Cam.SetDistance(Cam.GetDistance()*(1.0f-dx*rate));
	}
}

////////////////////////////////////////////////////////////////////////////////
