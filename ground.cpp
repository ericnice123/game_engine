#include "ground.h"


Ground::Ground(V3 pos_1, V3 pos_2, V3 pos_3, V3 pos_4)
{
    pos1 = pos_1;
    pos2 = pos_2;
    pos3 = pos_3;
    pos4 = pos_4;
}

void Ground::Draw()
{
    glDisable(GL_LIGHTING);
    glColor3d(0.54, 0.26, 0.074);
    glBegin(GL_QUADS);
    glNormal3f(0,1,0);
    glVertex3d(pos1.getX(), pos1.getY(), pos1.getZ());
    glVertex3d(pos2.getX(), pos2.getY(), pos2.getZ());
    glVertex3d(pos3.getX(), pos3.getY(), pos3.getZ());
    glVertex3d(pos4.getX(), pos4.getY(), pos4.getZ());
    glEnd();
    glEnable(GL_LIGHTING);
}