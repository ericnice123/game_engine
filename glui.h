#ifndef GLUI_H
#define GLUI_H

#include <GLUI/glui.h>
#include <GLUT/glut.h>
#include "Skeleton.h"

class Glui{
public:
    Glui(Skeleton* skeleton, int& window);
    static void myGlutIdle(void);
    
private:
    Skeleton* skeleton;
    int value;
};

#endif